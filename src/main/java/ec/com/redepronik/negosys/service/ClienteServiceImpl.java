package ec.com.redepronik.negosys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.redepronik.negosys.dao.ClienteDao;
import ec.com.redepronik.negosys.entity.Cliente;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteDao clienteDao;

	public Cliente obtenerFechaCortePorRuc(String ruc) {
		List<Cliente> list = clienteDao.obtenerPorHql(
				"select c from Cliente c where c.ruc=?1", new Object[] { ruc });
		return !list.isEmpty() ? list.get(0) : null;
	}

}