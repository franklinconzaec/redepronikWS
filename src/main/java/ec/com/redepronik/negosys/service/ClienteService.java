package ec.com.redepronik.negosys.service;

import org.springframework.transaction.annotation.Transactional;

import ec.com.redepronik.negosys.entity.Cliente;

public interface ClienteService {

	@Transactional
	public Cliente obtenerFechaCortePorRuc(String ruc);

}
