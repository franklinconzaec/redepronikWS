package ec.com.redepronik.negosys;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class AmazonS3File {

	private Bucket bucket = null;
	private String region = Regions.SA_EAST_1.getName();
	private AmazonS3 s3client = null;

	public AmazonS3File(String userAwsS3, String passAwsS3, String bucket) {
		this.s3client = new AmazonS3Client(new BasicAWSCredentials(userAwsS3,
				passAwsS3));
		this.bucket = new Bucket(bucket);
	}

	public Bucket crearBucket(String bucketName) {
		try {
			s3client.setRegion(Region.getRegion(Regions.fromName(region)));
			bucket = s3client.doesBucketExist(bucketName) ? new Bucket(
					bucketName) : s3client
					.createBucket(new CreateBucketRequest(bucketName, region));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bucket;
	}

	public String getBucketLocation() {
		try {
			return s3client.getBucketLocation(bucket.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public InputStream getObject(String archivo) {
		try {
			S3Object s3object = s3client.getObject(bucket.getName(), archivo);
			return s3object.getObjectContent();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void uploadObject(String archivo, File uploadArchivo) {
		try {
			s3client.putObject(bucket.getName(), archivo, uploadArchivo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void copyObject(String archivo, String destinoArchivo) {
		try {
			s3client.copyObject(bucket.getName(), archivo, bucket.getName(),
					destinoArchivo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<String> listArchivos(String prefix) {
		List<String> list = new ArrayList<String>();
		try {
			ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
					.withBucketName(bucket.getName()).withPrefix(prefix);
			ObjectListing objectListing;
			do {
				objectListing = s3client.listObjects(listObjectsRequest);
				for (S3ObjectSummary objectSummary : objectListing
						.getObjectSummaries()) {
					list.add(objectSummary.getKey());
					System.out.println(" - " + objectSummary.getKey() + "  "
							+ "(size = " + objectSummary.getSize() + ")");
				}
				listObjectsRequest.setMarker(objectListing.getNextMarker());
			} while (objectListing.isTruncated());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public void deleteObject(String archivo) {
		try {
			s3client.deleteObject(bucket.getName(), archivo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Boolean restoreObject(String archivo) {
		try {
			s3client.restoreObject(bucket.getName(), archivo, 2);
			ObjectMetadata response = s3client.getObjectMetadata(
					bucket.getName(), archivo);
			return response.getOngoingRestore();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void createFolder(String carpeta) {
		try {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(0);
			s3client.putObject(bucket.getName(), carpeta + "/",
					new ByteArrayInputStream(new byte[0]), metadata);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
