package ec.com.redepronik.negosys.dao;

import ec.com.redepronik.negosys.entity.Cliente;

public interface ClienteDao extends GenericDao<Cliente, Integer> {

}