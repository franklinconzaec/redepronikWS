package ec.com.redepronik.negosys.dao;

import org.springframework.stereotype.Repository;

import ec.com.redepronik.negosys.entity.Cliente;

@Repository
public class ClienteDaoImpl extends
		GenericDaoImpl<Cliente, Integer> implements ClienteDao {

}