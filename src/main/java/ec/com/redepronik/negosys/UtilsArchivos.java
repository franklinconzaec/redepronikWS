package ec.com.redepronik.negosys;

import java.io.File;
import java.io.FileOutputStream;

public class UtilsArchivos {

	public static File convertir(String nombre, String tipo, byte[] archivo) {
		try {
			File file = File.createTempFile(nombre, tipo);
			file.deleteOnExit();
			FileOutputStream fileOuputStream = new FileOutputStream(file);
			fileOuputStream.write(archivo);
			fileOuputStream.close();
			return file;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
