package ec.com.redepronik.negosys.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String nombre;
	private String ruc;
	private Date fechaInicio;
	private Date fechaCorte;

	public Cliente() {
	}

	public Cliente(Integer id, String nombre, String ruc, Date fechaInicio,
			Date fechaCorte) {
		this.id = id;
		this.nombre = nombre;
		this.ruc = ruc;
		this.fechaInicio = fechaInicio;
		this.fechaCorte = fechaCorte;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Column(name = "`fechaCorte`", nullable = false)
	public Date getFechaCorte() {
		return fechaCorte;
	}

	@Column(name = "`fechaInicio`", nullable = false)
	public Date getFechaInicio() {
		return fechaInicio;
	}

	@Id
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;

	}

	@Column(length = 50, nullable = false)
	public String getNombre() {
		return nombre;
	}

	@Column(length = 13, nullable = false)
	public String getRuc() {
		return ruc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

}