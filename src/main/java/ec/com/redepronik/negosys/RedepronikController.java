package ec.com.redepronik.negosys;

import static ec.com.redepronik.negosys.UtilsArchivos.convertir;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ec.com.redepronik.negosys.service.ClienteService;

@RestController
public class RedepronikController {

	@Autowired
	private ClienteService clienteService;

	@RequestMapping("/redepronikWS/saludar")
	public String saludar(@RequestParam(value = "nombre") String nombre) {
		return "hola mundo...!!!\n" + nombre;
	}

	@RequestMapping("/redepronikWS/fechaCorte")
	public String fechaCorte(@RequestParam(value = "ruc") String ruc) {
		return clienteService.obtenerFechaCortePorRuc(ruc).getFechaCorte()
				.toString();
	}

	@RequestMapping("/redepronikWS/almacenamiento")
	public void almacenamiento(
			@RequestParam(value = "userAwsS3") String userAwsS3,
			@RequestParam(value = "passAwsS3") String passAwsS3,
			@RequestParam(value = "bucketAwsS3") String bucketAwsS3,
			@RequestParam(value = "nombreArchivo") String nombreArchivo,
			@RequestParam(value = "xml") MultipartFile xml,
			@RequestParam(value = "ride") MultipartFile ride) {
		try {
			AmazonS3File amazonS3File = new AmazonS3File(userAwsS3, passAwsS3,
					bucketAwsS3);
			amazonS3File.uploadObject(nombreArchivo + ".xml",
					convertir(nombreArchivo, ".xml", xml.getBytes()));
			amazonS3File.uploadObject(nombreArchivo + ".pdf",
					convertir(nombreArchivo, ".pdf", ride.getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
